#!/usr/bin/env python

import sys, argparse, re, random

parser = argparse.ArgumentParser(description="generate events that maximise the activation for a given neuron")
parser.add_argument("input_project")
parser.add_argument("output_file")
parser.add_argument("-n", "--nevents", default=100000, type=int)
parser.add_argument("-j", "--mask-jets", action="store_true",
                    help="mask variables called jet*Pt/Eta/Phi and generate a random uniform distribution of the number of jets (only nescessary for non-recurrent NN)")
args = parser.parse_args()

import logging
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

import h5py
import numpy as np

from KerasROOTClassification.utils import (
    weighted_quantile,
    get_max_activation_events,
    create_random_event,
    get_ranges
)
from KerasROOTClassification import load_from_dir
import meme

meme.setOptions(deactivated=True)

input_project = args.input_project
output_file = args.output_file

c = load_from_dir(input_project)
c._load_data()

ranges, mask_probs = get_ranges(c.transform(c.x_train), [0.01, 0.99], c.w_train_tot, mask_value=c.mask_value, max_evts=10000)

def mask_uniform(x, mask_value, recurrent_field_idx):
    """
    Mask recurrent fields with a random (uniform) number of objects. Works in place.
    """
    for rec_idx in recurrent_field_idx:
        for evt in x:
            masked = False
            nobj = int(random.random()*(rec_idx.shape[1]+1))
            for obj_number, line_idx in enumerate(rec_idx.reshape(*rec_idx.shape[1:])):
                if obj_number == nobj:
                    masked=True
                if masked:
                    evt[line_idx] = mask_value

def get_input_flat(x):
    return x[0].reshape(-1, len(c.fields))


if args.mask_jets:
    jet_fields = {}
    for field_name in c.fields:
        if any(field_name.startswith("jet") and field_name.endswith(suffix) for suffix in ["Pt", "Eta", "Phi"]):
            jet_number = re.findall("[0-9]+", field_name)[0]
            if not jet_number in jet_fields:
                jet_fields[jet_number] = []
            jet_fields[jet_number].append(c.fields.index(field_name))
    jet_fields = [np.array([[v for k, v in sorted(jet_fields.items(), key=lambda l:l[0])]])]


def input_transform(x):
    x = np.array(x)
    if hasattr(c, "mask_uniform"):
        c.mask_uniform(x)
        return c.get_input_list(x)
    elif args.mask_jets:
        mask_uniform(x, c.mask_value, jet_fields)
        return x


opt_kwargs = dict()
if hasattr(c, "mask_uniform"):
    opt_kwargs["input_transform"] = input_transform
    opt_kwargs["input_inverse_transform"] = c.get_input_flat
if args.mask_jets:
    opt_kwargs["input_transform"] = input_transform
    opt_kwargs["input_inverse_transform"] = get_input_flat


evts = get_max_activation_events(
    c.model, ranges,
    ntries=args.nevents,
    layer=len(c.model.layers)-1,
    neuron=0,
    maxit=10,
    seed=45,
    threshold=0,
    **opt_kwargs
)

with h5py.File(output_file, "w") as f:
    f.create_dataset("actmax", data=evts[1])
